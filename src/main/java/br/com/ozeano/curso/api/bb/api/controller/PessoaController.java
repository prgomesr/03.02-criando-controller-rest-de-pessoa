package br.com.ozeano.curso.api.bb.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("pessoas")
@RestController
public class PessoaController {

	@GetMapping
	public String listar() {
		return "Você vai gerar um boleto em Java de forma muito profissional e atendendo a todos os padroes de projeto do mercado.";
	}
	
}
